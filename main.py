import os
import sys
import requests
from models import MergeRequestUrl, MergeRequest
from loguru import logger
import emoji


def get_mr(project_id: int, mr_iid: int, secret_token: str, api_url: str) -> MergeRequest:
    """Return MergeRequest dataclass"""
    h = {'PRIVATE-TOKEN': secret_token}
    # logger.debug('headers -> {}', h) # FIXME don't uncomment in CI!
    ep = f'{api_url}/projects/{project_id}/merge_requests/{mr_iid}'
    logger.debug('EP -> {}', ep)
    r: requests.models.Response = requests.get(ep, headers=h)
    logger.debug('status code {}', r.status_code)
    json: dict = r.json()
    return MergeRequest(**json)

def _get_env_var_or_ci_predefined_variable(ci_env: str = 'NOT_EXISTED_VALUE', env_name: str = 'NOT_EXISTED_VALUE') -> str | None: # is it good idea?
    # trying to fetch ci pred variable!
    try:
        ci_predefined_variable = os.environ['ci_env']
    except KeyError as e:
        logger.warning('Could not extract CI_ENV=<{}>', ci_env)
    # on successfully extraction
    else:
        return os.environ.get(env_name, ci_predefined_variable)
    # on failed extraction
    return os.environ.get(env_name)

def calculate_votes_diff(mr: MergeRequest, vote_to_merge: int = 1):
    delta = mr.upvotes - mr.downvotes
    logger.debug('Upvotes {}', mr.upvotes )
    logger.debug('Downvotes {}', mr.downvotes)
    logger.debug('Delta {}', delta)
    logger.debug('Need {} type=<{}>', vote_to_merge, type(vote_to_merge))

    # logger.debug('Need {} type=<{}> {} votes, to merge; delta = {}', vote_to_merge, type(vote_to_merge), emoji.emojize(':thumbs_up:'), delta)
    if delta < vote_to_merge:
        logger.error("{} -> Unf, it couldn't be merged, ask for more likes", emoji.emojize(':imp:'))
        sys.exit(1)
    else:
        logger.info("{} -> pipeline could be merged, good luck!", emoji.emojize(':thumbs_up:'))


def main(secret_token: str):
    if not isinstance(secret_token, str):
        logger.critical('{} SECRET_TOKEN_TYPE IS NOT STR type=<{}>', emoji.emojize(':fire:'), type(secret_token))
    mr_function_pload = {
        'api_url': os.environ.get('CI_API_V4_URL'),
        'project_id': os.environ.get('CI_PROJECT_ID'),
        'mr_iid': os.environ.get('CI_MERGE_REQUEST_IID'),
        'secret_token': secret_token,
    }
    # logger.debug('pload for request function with token {}', mr_function_pload) # FIXME don't uncomment in CI!
    mr = get_mr(**mr_function_pload)
    # only one like by default
    vote_to_merge = int(os.environ.get('VOTES_TO_MERGE', 1))
    calculate_votes_diff(mr, vote_to_merge)

if __name__ == '__main__':
    mr = main(os.environ.get('SECRET_TOKEN'))
