from dataclasses import dataclass
from datetime import datetime
from pydantic import BaseModel
import typing

class User(BaseModel):
    """User repr"""
    id: int
    name: str
    username: str
    state: str # TODO should be own object!
    avatar_url: str # TODO should be url object
    web_url: str # TODO should be url object


class Author(User):
    """author repr"""

class Pipeline(BaseModel):
    id: int
    project_id: int # TODO own object?
    sha: str # TODO OWN OBJECT
    ref: str # TODO OWN OBJECT
    status: str # TODO failed, succesfull?
    source: str # TODO OWN OBJECT!!!
    created_at: datetime
    updated_at: datetime
    web_url: str # TODO object?

class HeadPipeline(Pipeline):
    before_sha: str # TODO what is this?
    tag: bool # TODO is it ok? bool | list[str] ?
    yaml_errors: bool
    user: User

class MergeRequestState(BaseModel):
    """state repr"""

class References(BaseModel):
    short: str
    relative: str
    full: str # TODO own object?

class TimeStats(BaseModel):
    """repr for field"""




class DetailedStatus(BaseModel):
    """head_pipeline -> repr"""

class MergeRequestUrl(BaseModel):
    """class for URL for specific MR"""
    ci_apiv4_url: str # TODO add validation or WEB URL OBJECT
    project_id: int # TODO own object
    mr_iid: int # TODO is it ok?





class MergeRequest(BaseModel):
    """Object from JSON repr"""
    id: int
    iid: int # TODO own class?
    project_id: int
    title: str
    description: str
    state: str  # TODO - create own representation for this object
    created_at: datetime
    updated_at: datetime
    merged_by: typing.Union[None, User] # TODO - check othe options
    merged_at: typing.Union[None, datetime] # TODO - check other option
    closed_by: typing.Union[None, User] # TODO - check other option
    closed_at: typing.Union[None, User]
    target_branch: str # TODO - should be a branch
    source_branch: str # TODO - should be a branch
    user_notes_count: int
    upvotes: int
    downvotes: int
    author: User
    assignees: list # TODO list[Author] ?
    assignee: typing.Union[None, User] # ?
    reviewers: list # TODO list[Author]
    source_project_id: int
    target_project_id: int
    labels: list # TODO list[str]?
    draft: bool
    work_in_progress: bool
    milestone: None # TODO read about it
    merge_when_pipeline_succeeds: bool
    merge_status: str # TODO own object
    sha: str # TODO sha object
    merge_commit_sha: typing.Union[None, str] # TODO sha object
    squash_commit_sha: typing.Union[None, str] # TODO sha object
    discussion_locked: None
    should_remove_source_branch: None
    force_remove_source_branch: bool
    reference: str # TODO Reference
    references: dict # TODO References?
    web_url: str # TODO url object?
    time_stats: dict # TODO TimeStats object
    squash: bool
    task_completion_status: dict
    has_conflicts: bool
    blocking_discussions_resolved: bool
    approvals_before_merge: None
    subscribed: bool
    changes_count: typing.Union[str, int] # TODO is it okay?
    latest_build_started_at: typing.Union[None, datetime]
    latest_build_finished_at: typing.Union[None, datetime]
    first_deployed_to_production_at: typing.Union[None, datetime]
    pipeline: Pipeline
    started_at: typing.Union[None, datetime]
    finished_at: typing.Union[None, datetime]
    committed_at: typing.Union[None, datetime] # TODO bool | datetime?
    # duration: int
    # queued_duration: int
    # coverage: None # TODO is it okay?
    # detailed_status: dict # TODO OWN OBJECT
    # diff_refs: dict # TODO some sha
    # merge_error: bool
    first_contribution: bool
    user: dict # TODO look like its about permissions
